import Vue from 'vue'
import Router from 'vue-router'
import home from '@/components/home.vue'
import data from '@/components/userdata.vue'
import error from '@/components/error.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: home
    },
    {
      path: '/userdata',
      name: 'data',
      component: data
    },
    {
      path: '/error',
      name: 'error',
      component: error
    },
    {
      path: '/*',
      name: 'error2',
      component: error
    },
  ]
})
