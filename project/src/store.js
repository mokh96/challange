import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    showlog:false,
    loggedin:false,
    username:""
  },
  mutations: {
    setlog(state, show) {
      state.showlog = show
    },
    setlogged(state, set) {
      state.loggedin = set
    },
    setuser(state, user) {
      state.username = user
    },
  },
  getters:{
    getlog:state=>state.showlog,
    getlogged:state=>state.loggedin,
    getuser:state=>state.username,
  },
  actions: {

  }
})
