const nano = require('nano')('http://localhost:5984');

const dbase = nano.db.use('fun');
const bcrypt = require('bcrypt');

module.exports = {
  login(req, res, next) {
    dbase.view('des', 'getusers',{key:req.body.username}, (err, body) => {
      if (!err) {
        if (body.rows.length==0){
          res.send("invalid");
        }else {
          bcrypt.compare(req.body.password, body.rows[0].value.password, function(err, result) {
            if(result){
              var data=["success",body.rows[0].value.current]
              res.send(data);
            }else 
              res.send("invalid");
        });

        }
      }
    });
    next();
  },
};
