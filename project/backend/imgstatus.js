const nano = require('nano')('http://localhost:5984');

const dbase = nano.db.use('fun');

module.exports = {
  update(req, res, next) {
    dbase.view('des', 'getusers',{key:req.body.username}, (err, body) => {
      if (!err) {
        if (body.rows.length==0){
          res.send("error")
        }else {
          if (body.rows[0].value.imgs[req.body.id]==req.body.status){
            delete body.rows[0].value.imgs[req.body.id];
          }else{
          body.rows[0].value.imgs[req.body.id]=req.body.status;
        }
        dbase.insert(body.rows[0].value).then((body1) => {
          res.send("success");
        });
      }
      }
    });
    next();
  },
};
