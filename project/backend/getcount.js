const nano = require('nano')('http://localhost:5984');

const dbase = nano.db.use('fun');

module.exports = {
  getcount(req, res, next) {
    dbase.view('des', 'getimages', (err, body) => {
      if (!err) {
        if (body.rows.length==0){
          res.send("error")
        }else
        var id= req.body.id;
        var status=req.body.status;
        var action=req.body.action;
        var neg;
        if (status){ 
          var l=0;
          var d=0;
          if (status=="like") {
            l=1;
            neg="dislike";
          }
          else if (status=="dislike") {
            d=1;
            neg="like";
          }
          var obj={"like":l,"dislike":d}
          if(body.rows[0].value.images[id]){
            if (action=="add")
              body.rows[0].value.images[id][status]= parseInt(body.rows[0].value.images[id][status])+1;
            else if (action=="sub")
              body.rows[0].value.images[id][status]= parseInt(body.rows[0].value.images[id][status])-1;
            else if (action=="change"){
              body.rows[0].value.images[id][status]= parseInt(body.rows[0].value.images[id][status])+1;
              body.rows[0].value.images[id][neg]= parseInt(body.rows[0].value.images[id][neg])-1;
            }
          }else {
            body.rows[0].value.images[id]=obj;
          }
          dbase.insert(body.rows[0].value).then((body1) => {
          });
        }
         if(body.rows[0].value.images[id])
           res.send(body.rows[0].value.images[id])
        else res.send({"like":0,"dislike":0})
      }
    });
  next();
  },
};
