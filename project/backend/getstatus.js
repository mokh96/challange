const nano = require('nano')('http://localhost:5984');

const dbase = nano.db.use('fun');

module.exports = {
  getstatus(req, res, next) {
    dbase.view('des', 'getusers',{key:req.body.username}, (err, body) => {
      if (!err) {
        if (body.rows.length==0){
          res.send("error")
        }else {
          if (body.rows[0].value.imgs[req.body.id])
            res.send(body.rows[0].value.imgs[req.body.id]);
          else res.send("empty")
        }
      }
    });
    next();
  },
};
