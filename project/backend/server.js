const restify = require('restify');
const plug = require('restify-plugins');
const corsMiddleware = require('restify-cors-middleware');

const server = restify.createServer();
const create = require('./createuser.js');
const login = require('./login.js');
const update = require('./imgstatus.js');
const inc = require('./increment.js');
const getstatus = require('./getstatus.js');
const getpos = require('./getpos.js');
const getimgs = require('./getimgs.js');
const getcount = require('./getcount.js');
const cors = corsMiddleware({
 
  origins: ['*'],
  allowHeaders: ['*']
})
 
server.pre(cors.preflight)
server.use(cors.actual)

server.use(plug.bodyParser());
server.listen(8888, () => {
  console.log('ready on %s', server.url);
});

server.post('/createuser', create.create);
server.post('/login', login.login);
server.post('/status', update.update);
server.post('/inc', inc.inc);
server.post('/getstatus', getstatus.getstatus);
server.post('/getpos', getpos.getpos);
server.post('/getimgs', getimgs.getimgs);
server.post('/getcount', getcount.getcount);