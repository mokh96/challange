-install dependencies by "npm install"
-generate database by running "node generate-db" from backend folder
-run server by running "node server" from backend folder
-run project by running "npm run serve"
-all basic requirements are implemented
-additional features include:
    -keeping track of decision on the image and desplaying it
    -keep track of collective number of likes/dislikes on images for all users
    -if user logout or leaves they can continue from the place they left
    -in userdata user can view liked/disliked images in addition to their ids
    -user can unlike or undislike image
-CORS error was avoided by running chrome through a batch file:
~~~    
    TASKKILL /F /IM chrome.exe
start chrome.exe --args --disable-web-security --user-data-dir=c:\testfolder
pause
~~~
